#!//usr/bin/ruby
require 'asciidoctor'

html = Asciidoctor.convert_file 'public/lab-guide.adoc', to_file: false, header_footer: true
html.gsub!('<link rel="stylesheet" href="./styles.css">', '<link rel="stylesheet" href="https://unpkg.com/picnic"/><link rel="stylesheet" href="styles.css"/>')
File.write('public/index.html', html)
puts 'Generated public/index.html'