# Hands on with Satellite 6.7

https://redhatsummitlabs.gitlab.io/hands-on-with-satellite-6-7/

Official site for Red Hat Summit's Satellite 6.7 lab documentation

## Development

Prerequisites
- Requires 'asciidoctor' and 'ruby', tested on `asciidoctor-1.5.6.1-6`

To update site:
- fork and clone project
- update `public/lab-guide.adoc` with your changes
- `yum install rubygem-asciidoctor`
- Run `./generate-html.rb`
- html will be in `public/index.html`
- Make an MR and merge
- The site is auto-deployed when merged to master
